$( document ).ready(function() {
  
	//server
	var server = "http://localhost:8080/22_Backend";
     
    onLoad();
	  
	//addEventListener 
	$('#myForm').submit( processForm );

  	// onLoad FUNCTION;
	function onLoad(){

		$('#success').hide();
		$('#error').hide();

		var country =  $('#country');
		$.ajax({  
	        url: server + '/GetCountries', 
	        dataType: 'json',
	        success: function (items) { 
	            $.each(items.countries, function(key, val) {
					country.append('<option value="' + val.code + '">' + val.name + '</option>');
				})
	        }
    	});

	}

	function processForm(e) {
        e.preventDefault(); //stop the browser;

		//CLEAN ERROR 
		$('#error').hide();

		// Get html reference
		var nameuser = $('#nameuser').val();
		var birthday = $('#birthday').val();
		var country = $('#country').val();
		var username = $('#username').val();
		var email = $('#email').val();
		var country = $('#country').val();
		var password = $('#password').val();
		var verifypassword = $('#verifypassword').val();
		 
		if (password != verifypassword) {
			$('#error').show();
		} else{

			//Create OBJ
			var objtosend = {
				nameuser: nameuser,birthday: birthday,country: country,username: username,email: email, password:password
			}

			console.log(objtosend);

			$.ajax({
				dataType: 'json',
				type:'POST',
				url: server + '/CreateUser',
				data: objtosend, 
			}).done(function(response){
				console.log(response);
				$('#error').hide();
				$('#myForm').hide();
				$('#success').show();
			});  		

		}		
	}
 


}); // end document ready

 

 